/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apachepoi;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aluno
 */
public class Main {
        /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Aluno> listaAlunos = new ArrayList<>();
        listaAlunos.add(new Aluno("Eduardo", "9876525", 7, 8, 0, false));
        listaAlunos.add(new Aluno("Luiz", "1234466", 5, 8, 0, false));
        listaAlunos.add(new Aluno("Bruna", "6545657", 7, 6, 0, false));
        listaAlunos.add(new Aluno("Carlos", "3456558", 10, 3, 0, false));
        listaAlunos.add(new Aluno("Sonia", "6544546", 7, 8, 0, false));
        listaAlunos.add(new Aluno("Brianda", "3234535", 6, 5, 0, false));
        listaAlunos.add(new Aluno("Pedro", "4234524", 7, 5, 0, false));
        listaAlunos.add(new Aluno("Julio", "5434513", 7, 2, 0, false));
        listaAlunos.add(new Aluno("Henrique", "6543452", 7, 8, 0, false));
        listaAlunos.add(new Aluno("Fernando", "4345651", 5, 8, 0, false)); 
        listaAlunos.add(new Aluno("Vitor", "4332341", 7, 9, 0, false));
    
        ApachePoi.criaExcel(listaAlunos, "./aluno.xls");
    
    }
}
