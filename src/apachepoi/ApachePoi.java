/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apachepoi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author aluno
 */
public class ApachePoi {

    public static <T> void criaExcel(final List<T> lista, final String nomeArquivo) {

        try {
            final HSSFWorkbook workbook = new HSSFWorkbook();
            final HSSFSheet sheetAlunos = workbook.createSheet(
                            lista.get(0).getClass().getName());
            final int rowTitulo = 0;
            final Row titulo = sheetAlunos.createRow(rowTitulo);
            int tituloNum = 0;
            for (final Field campoObjeto : lista.get(0).getClass().getDeclaredFields()) {
                if (!campoObjeto.getName().equalsIgnoreCase("serialVersionUID")) {

                    titulo.createCell(tituloNum++).setCellValue(campoObjeto.getName().toUpperCase());
                    System.out.println(campoObjeto.getType());
                }
            }

            int rowNum = 1;
            for (final T element : lista) {
                final Row row = sheetAlunos.createRow(rowNum++);
                int cellNum = 0;
                for (final Field campo : element.getClass().getDeclaredFields()) {
                    if (!campo.getName().equalsIgnoreCase("serialVersionUID")) {

                        try {
                            campo.setAccessible(true);
                            Object object = campo.get(element);

                            if (object == null) {

                                object = "";
                            }

                            row.createCell(cellNum++).setCellValue(object.toString());

                        } catch (IllegalArgumentException | IllegalAccessException ex) {
                            Logger.getLogger(ApachePoi.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

            final FileOutputStream fileOutputStream = new FileOutputStream(new File(nomeArquivo));

            workbook.write(fileOutputStream);

            fileOutputStream.close();
        } catch (final FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Arquivo não encontrado");
        } catch (final IOException ex) {
            System.out.println("Erro na edicao do arquivo");
        }
    }
}
